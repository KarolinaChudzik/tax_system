import requests
import json
import re
from bs4 import BeautifulSoup

URL = 'https://tax-legal-cdn.s3-eu-west-1.amazonaws.com/tax-rates/public/tax-rates-table.html?taxtype=1'


class TaxRateCheck():

    def __init__(self):
        self.link = ''
        self.link_two = ''
        self.tax_rates = {}
        self.tax_rate = 0

    def first_page(self):
        r = requests.get(URL)
        page_data = r.text
        soup = BeautifulSoup(page_data, 'html.parser')
        scripts = soup.find_all('script')
        srcs = [link['src'] for link in scripts if 'src' in link.attrs]
        src = srcs[-1]
        self.link = src.replace('../', 'https://tax-legal-cdn.s3-eu-west-1.amazonaws.com/tax-rates/')

    def second_page(self):
        r_src = requests.get(self.link)
        data = re.search(r'"([^"]+taxdata\.json)"', str(r_src.text))
        self.link_two = data.group(1)

    def third_page(self):
        r_data = requests.get(self.link_two)
        r_data.encoding='utf-8-sig'
        data_page = json.loads(r_data.text)
        self.tax_rates = data_page['graphs']['tax'][0]['data']

    def check_country(self, countries, years):
        for country in self.tax_rates:
            if country['name'] == countries:
                for year in country['values']:
                    if year['year'] == years:
                        self.tax_rate = year['value']