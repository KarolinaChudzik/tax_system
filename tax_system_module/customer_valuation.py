import csv

class CustomerValuation:

    def __init__(self):
        self.customer_erosion_rate = 0
        self.sales_per_year = {} # customer sales per year
        self.attributed_revenue_per_year = {} # revenue attributed to existing distribution rights per year
        self.return_attributed_per_year = {}
        self.year_start = 0
        self.after_tax_return_per_year = {} # after tax return attributed to distribution rights per year
        self.present_value_distribution_per_year = {} 
        self.value_distr_rights = 0
        self.period_cal = 0
        self.discount_rate = 0
        self.tax_rate = 0
        self.amortization_benefit = 0
        self.fair_values = 0

    def sales_value(self, sales, growth, start_year, cal_period): 
        """First setp - calculation of sales value for each year."""
        sales_value = float(sales)
        growth_rate = float(growth)
        self.year_start = int(start_year)
        self.period_cal = int(cal_period)
        sales_in_year = 0 
        for i in range(self.period_cal):
            current_year = self.year_start + i
            if i == 0:
                sales_in_year += sales_value
            else:
                growth_value = sales_in_year * (growth_rate / 100)
                sales_in_year += growth_value
            self.sales_per_year[current_year] = sales_in_year

    def customer_erosion(self, checks, customer_start, customer_ends, erosion_rates): 
        """Second step - calculation of customer erosion rate used for calculation of 
        Revenue attributed to existing destribution rights"""
        check = str(checks)
        if check == 'no':
            customer_begining = int(customer_start)
            customer_end = int(customer_ends)
            self.customer_erosion_rate = (customer_begining - customer_end) / customer_begining
        elif check == 'yes':
            self.customer_erosion_rate = float(erosion_rates)
        else:
            print('Incorrect value were entered, please try again.')

    def attributed_revenue(self):
        """Third step - revenue attributed to existing distribution right, 
        sales value multiply by customer erosion"""
        erosion = 0
        existing_distribution_rights = 0
        for key, value in self.sales_per_year.items():
            if key == self.year_start:
                erosion_change = (1 - self.customer_erosion_rate)
                erosion += erosion_change
                existing_distribution_rights += (value * erosion)
            else:
                erosion_change = erosion * self.customer_erosion_rate
                erosion += - erosion_change
                existing_distribution_rights += (value * erosion)
            self.attributed_revenue_per_year[key] = existing_distribution_rights

    def return_attributed(self, mark_ups):
        """Fourth step - calculation of return attributed to existing distribution rights"""
        mark_up = float(mark_ups)
        for key, value in self.attributed_revenue_per_year.items():
            distribution_rights = value * mark_up
            self.return_attributed_per_year[key] = distribution_rights

    def after_tax_return(self, tax_rates):
        """Fifth step - calculation of after tax value, based on tax rate."""
        self.tax_rate = float(tax_rates)
        for key, value in self.return_attributed_per_year.items():
            taxes= value * self.tax_rate
            after_tax_return = value = taxes
            self.after_tax_return_per_year[key] = after_tax_return

    def present_value_distribution(self, discounts):
        """Sixth step - calculation of present value of distribution rights 
        based on discounted period and discount rate"""
        periods_to_discount = 0
        self.discount_rate = float(discounts)
        for key, value in self.after_tax_return_per_year.items():
            periods_to_discount += 1
            present_value = value/((1+self.discount_rate)**periods_to_discount)
            self.present_value_distribution_per_year[key] = present_value
        
    def value_distribution_right(self):
        """Seventh step - sum of PV distribution right"""
        for key, value in self.present_value_distribution_per_year.items():
            self.value_distr_rights += value
        
    def tax_amortization(self):
        """Eight step - tax amortization benefit calculation using formula:
        AB = PVCF * (n/(n-(PVA_T))-1) where n - amortization period, PVCF - 
        PVA - present value of an annuity, T - tax rate.
        Formula for PVCF = Payment Value ((1-(1/(1+Dr)^Number of Periods))/Dr) 
        Formula for PVA = $1 ((1-(1/(1+Dr)^n))/Dr) where Dr - Discount Rate. """
        # Formula to be validated
        PVCF = self.value_distr_rights * ((1-(1/(1+self.discount_rate) ** self.period_cal))/self.discount_rate)
        PVA = 1*((1-(1/(1+self.discount_rate) ** self.period_cal))/self.discount_rate)
        self.amortization_benefit = -PVCF * (self.period_cal/(self.period_cal-(PVA/self.tax_rate))-1) #SPRAWDZIC, POWINNO BYC PVA_T

        #self.amortization_benefit = -self.value_distr_rights (self.period_cal/(self.period_cal-self.tax_rate * np.pv(self.discount_rate, self.period_cal, 1))-1)

    def fair_value(self):
        """Final step - calculation of fair value of customer relationship."""
        self.fair_values = self.value_distr_rights + self.amortization_benefit

    def save_result(self):
        with open("output_data.csv", "w") as file:
            for item in self.fair_values:
                file.write(str(item) + "\n")      





