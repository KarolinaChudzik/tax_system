from django.urls import path
from . import views

urlpatterns = [
    path("", views.project_index, name="project_index"),
    path("", views.project_index, name="project_index"),
    path("detail/", views.project_detail, name="project_detail"),
    path("precheck/", views.project_precheck, name="project_precheck"),
    path("result/", views.project_result, name="project_result"),
    path("taxrate", views.project_taxrate, name="project_taxrate"),
    path("export", views.export, name="export")

]