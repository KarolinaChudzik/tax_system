from django import forms
from django.http import response
from django.shortcuts import render
from projects.models import Project
from projects.forms import CustomerValuation
from customer_valuation import CustomerValuation
from django.http import FileResponse, HttpResponse
from tax_rate import TaxRateCheck
import csv
import io
from reportlab.pdfgen import canvas, textobject
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter

def project_home(request):
    projects = Project.objects.all()
    context = {
        'projects': projects
    }
    return render(request, 'project_index.html', context)

def project_index(request):
    if request.POST.get('checks'):
        return project_taxrate(request)
    if request.POST.get('taxrate_check'):
        return project_detail(request)
    if request.GET.get('precheck'):
        return project_precheck(request)
    if request.POST.get('sales_value'):
        return project_result(request)
    return project_home(request)

def project_precheck(request):
    projects = Project.objects.all()
    context = {}
    return render(request, 'project_precheck.html', context)

def project_taxrate(request):
    my_form = Project.objects.all()
    country = request.POST.get('country')
    request.session['country'] = country
    years = request.POST.get('year')
    request.session['year'] = years
    checks = request.POST.get('checks')
    request.session['checks'] = checks
    print(checks, 'taxrate checks')

    src = TaxRateCheck()
    if country:
        src.first_page()
        src.second_page()
        src.third_page()
        src.check_country(country, years)
        src.tax_rate
        request.session['tax_rate'] = src.tax_rate

    context = {
        'tax_rate': src.tax_rate,
        'check': checks, 
    }
    return render(request, 'project_taxrate.html', context)


def project_detail(request, *args, **kwargs):
    my_form = Project.objects.all()
    taxrate_check = request.POST.get('taxrate_check')
    request.session['taxrate_check'] = taxrate_check
    checks = request.session['checks']
    context = {
        'taxrate_check': taxrate_check,
        'check': checks, 
    }
    return render(request, 'project_detail.html', context)


def project_result(request):
    projects = Project.objects.all()
    cv = CustomerValuation()

    sales = request.POST.get('sales_value')
    growth = request.POST.get('growth_rate')
    start_year = request.session['year']
    cal_period = request.POST.get('years')
    check = request.session['checks']
    customer_start = request.POST.get('customer_begining')
    customer_ends = request.POST.get('customer_end')
    erosion_rates = request.POST.get('erosion')
    mark_ups = request.POST.get('mrkup')  
    discounts = request.POST.get('discounts')
    country = request.session['country']
    taxrate_check = request.session['taxrate_check']
    if taxrate_check == 'yes':
        tax_rates = request.session['tax_rate']
    else:
        tax_rates = request.POST.get('tax_rate')

    ins = Project(
        sales_value=sales,
        growth_rate=growth,
        year_start=start_year,
        period_cal=cal_period,
        check=check,
        customer_begining=customer_start,
        customer_end=customer_ends,
        customer_erosion_rate=erosion_rates,
        mark_up=mark_ups,
        tax_rate=tax_rates,
        discount_rate=discounts,
        country=country
        )
    ins.save()

    if sales:
        cv.sales_value(sales, growth, start_year, cal_period)
        cv.customer_erosion(check, customer_start, customer_ends, erosion_rates)
        cv.attributed_revenue()
        cv.return_attributed(mark_ups)
        cv.after_tax_return(tax_rates)
        cv.present_value_distribution(discounts)
        cv.value_distribution_right()
        cv.tax_amortization()
        cv.fair_value()
        request.session['fair_values']=cv.fair_values

    context = {
        'erosion_check': check,
        'value': round(cv.fair_values, 4), 
        'sales': cv.sales_per_year,
        'revenue': cv.attributed_revenue_per_year,
        'return': cv.return_attributed_per_year, 
        'present': cv.present_value_distribution_per_year,
        'distr_rigth': cv.value_distr_rights, 
        'amortization': cv.amortization_benefit
    }

    return render(request, 'project_result.html', context)

def export(request):
    projects = Project.objects.all()
    buf = io.BytesIO()
    c = canvas.Canvas(buf, pagesize=letter, bottomup=0)
    textob = c.beginText()
    textob.setTextOrigin(inch, inch)
    textob.setFont("Helvetica", 10)

    lines = [
        f"Country: {request.session['country']}",
        f"First year for calculation: {request.session['year']}", 
        f"Tax Rate used: {request.session['tax_rate']}", 
        f"Customer fair value: {request.session['fair_values']}",
    ]

    for line in lines:
        textob.textLine(line)

    c.drawText(textob)
    c.showPage
    c.save()
    buf.seek(0)

    return FileResponse(buf, as_attachment=True, filename="CustomerValuationDraft")
    