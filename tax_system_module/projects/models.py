from django.db import models

class Project(models.Model):
    sales_value = models.CharField(max_length=300, default=00000)
    growth_rate = models.CharField(max_length=300, default=00000)
    year_start = models.CharField(max_length=300, default=00000)
    period_cal = models.CharField(max_length=300, default=00000)
    check = models.TextField(max_length=300, default=00000)
    customer_begining = models.CharField(max_length=300, default=00000, null=True)
    customer_end = models.CharField(max_length=300, default=00000, null=True)
    customer_erosion_rate = models.CharField(max_length=300, default=00000, null=True)
    mark_up = models.CharField(max_length=300, default=00000)
    tax_rate = models.CharField(max_length=300, default=00000)
    discount_rate = models.CharField(max_length=300, default=00000)
    country = models.TextField(max_length=300, default=00000)
