from django.contrib import admin

# Register your models here.
from django.contrib import admin
from projects.models import Project

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        'sales_value', 'growth_rate', 'year_start', 'period_cal', 'check',
        'customer_begining', 'customer_end', 'customer_erosion_rate', 
        'mark_up', 'tax_rate', 'discount_rate', 'country'
        ]

"""    list_editable = [
        'sales_value', 'growth_rate', 'year_start', 'period_cal', 'check',
        'customer_begining', 'customer_end', 'customer_erosion_rate', 
        'mark_up', 'tax_rate', 'discount_rate'
        ]"""


# mozna definiowac szczegoly co i jak bedzie wyswietlnae